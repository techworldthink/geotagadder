import os
from exif import Image
import platform

# image generated folder location
output_img_folder = "geotag/"

if not platform.system() == "Windows":
    output_img_folder = "geotag\\"


# print metadata of images
def print_metadata(img_path):
    print("--------------------------------------")
    print(img_path)
    with open(img_path, 'rb') as img_file:
        img = Image(img_file)
        #print(img.has_exif)
        print([img.get(tag) for tag in sorted(img.list_all())])
    print("--------------------------------------")


# add geo tag to image
def geo_tag_adder(img_path,image_name, lat, lon,alt):
    # output image filename with relative path
    destination = output_img_folder+image_name

    # check the image given found or not 
    if not os.path.exists(img_path+image_name):
        print("Image not Found. Image Name:", image_name)
        print("")
        return 0
    
    # write geo tags
    with open(img_path+image_name, 'rb') as img_file:
        img = Image(img_file)
        img.gps_longitude = lon
        img.gps_latitude = lat
        img.gps_altitude = alt
        # save copy of image in destination folder
        with open(destination, 'wb') as new_image_file:
            new_image_file.write(img.get_file())

    #print("Image : " + destination)


def main():
    # check the destination folder for the newly created images already exist or not.
    # if not exist create it.
    if not os.path.exists(output_img_folder):
        os.mkdir(output_img_folder)
        print("Creating Folder: "+output_img_folder+", your geotagged images will be saved there")


    # input location.txt file (converted from the bin file from mission planner)
    location_file = input("Enter location.txt file location : ")
    if not os.path.isfile(location_file):
        print("Location.txt not found")
        exit()

    # input images directory
    img_location = input("Enter Images dir location : ")
    if not os.path.isdir(img_location):
        print("Image directory not found")
        exit()

    # read data from location.txt file
    file = open(location_file).readlines()

    for lines in file:
        if lines.startswith("#"):
            continue
        else:
            lines = lines.split(" ")

        # add geo tags to image (Longitude,Latitude,Altitude)
        geo_tag_adder(img_location,str(lines[0]), float(lines[1]), float(lines[2]), float(lines[3]))
        # print metadata of newly created images
        print_metadata(output_img_folder+str(lines[0]))


if __name__ == "__main__":
    main()