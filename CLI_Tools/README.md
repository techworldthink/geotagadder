# GeoTagAdder CLI tool

## How to use?

Run the python file named "geotagadder_cli.py"

Input the "location.txt" file (eg: C:\Logs\location.txt)

Input the directory that contain images that needs to be tag gps data (eg : C:\Images\)

Check the newly created "geotag" folder in the current directory. 