from tkinter.ttk import *
from tkinter import*
from tkinter import font
from tkinter.scrolledtext import ScrolledText
import tkinter as tk
from tkinter.filedialog import askopenfilename
from tkinter.messagebox import showinfo
import os
from os.path import exists
from tkinter.filedialog  import askdirectory
from exif import Image
import platform


dir_seperator = "/"

if not platform.system() == "Windows":
    output_img_folder = "\\"

root = Tk()

class Convert:
    def __init__(self, master):
        self.master = master
        master.title("Single Button Event")
        master.geometry("1000x500")
        master.configure(bg="white")
          
        #full window row configure
        master.grid_rowconfigure(0, weight=1)
   
        #full window column configure
        master.columnconfigure(0, weight=1)
        master.columnconfigure(1, weight=1)



        self.location_file = "/"
        self.image_dir = "/"
        self.destination_dir = "/"

        
        #Fonts
        self.label_frame_font = font.Font(family="Helvetica",size=10,weight="bold")
        self.frame2_font = font.Font(family="Franklin Gothic Medium",size=10)

        # MENU SECTION STARTED #####################################################
        self.menu_section = Menu(master)                                                              
        self.menu_section_file = Menu(self.menu_section,tearoff = 0)                         
        self.menu_section_file.add_command(label="About")
        self.menu_section_file.add_command(label="close")
        self.menu_section_file.add_separator()                                                           
        self.menu_section_file.add_command(label="Exit")
        master.config(menu = self.menu_section)                                                      
        self.menu_section.add_cascade(label="File", menu = self. menu_section_file)   

        #END MENU #####################################################################

        #labelled frames - master
        self.frame_left     =  LabelFrame(master,text="",labelanchor="n",bg="white",bd=0,fg="red",font=self.label_frame_font)
        self.frame_right    =  LabelFrame(master,text="Logs",labelanchor="n",bg="white",bd=0,fg="black",font=self.label_frame_font)

        #frame grids
        self.frame_left.grid(row=0,column=0,sticky="nsew")
        self.frame_right.grid(row=0,column=1,sticky="nsew")
        
        #frame for componants for fisrt labeled frame  row configure  1
        self.frame_left.grid_rowconfigure(0, weight=1)
        self.frame_left.grid_rowconfigure(1, weight=1)
        self.frame_left.columnconfigure(0, weight=1)

        #labelled frames - frame_left
        self.frame_left_one     =  LabelFrame(self.frame_left,text="Add metadata",labelanchor="n",bg="black",bd=0,fg="white",font=self.label_frame_font)
        self.frame_left_two    =  LabelFrame(self.frame_left,text="View metadata",labelanchor="n",bg="black",bd=0,fg="white",font=self.label_frame_font)

        #frame grids 1
        self.frame_left_one.grid(row=0,column=0,sticky="nsew")
        self.frame_left_two.grid(row=1,column=0,sticky="nsew")

        #frame for componants for left fisrt labeled frame  1.1
        self.frame_left_one.grid_rowconfigure(0, weight=1)
        self.frame_left_one.grid_rowconfigure(1, weight=1)
        self.frame_left_one.grid_rowconfigure(2, weight=1)
        self.frame_left_one.grid_rowconfigure(3, weight=1)
        self.frame_left_one.columnconfigure(0, weight=1)
        #componants for frame 1.1
        self.frame_left_btn1 = Button(self.frame_left_one,text="Load location.txt file",height = 3, width = 20,command=lambda:select_location_txt())
        self.frame_left_btn2 = Button(self.frame_left_one,text="Select Image directory",height = 3, width =20,command=lambda:choose_img_directory())
        self.frame_left_btn3 = Button(self.frame_left_one,text="Select destination directory",height = 3, width =20,command=lambda:choose_dest_directory())
        self.frame_left_btn4 = Button(self.frame_left_one,text="Add Geo Tag",height = 3, width =20,command=lambda:add_geotag())
        #componants grid for frame 1.1
        self.frame_left_btn1.grid(row=0,column=0,sticky="nsew")
        self.frame_left_btn2.grid(row=1,column=0,sticky="nsew")
        self.frame_left_btn3.grid(row=2,column=0,sticky="nsew")
        self.frame_left_btn4.grid(row=3,column=0,sticky="nsew")

        #frame for componants for left fisrt labeled frame  1.2
        self.frame_left_two.grid_rowconfigure(0, weight=1)
        self.frame_left_two.grid_rowconfigure(1, weight=1)
        self.frame_left_two.columnconfigure(0, weight=1)
        #componants for frame 1.2
        self.frame_left_btn12 = Button(self.frame_left_two,text="Select Image directory",height = 3, width = 20,command=lambda:choose_dest_directory())
        self.frame_left_btn22 = Button(self.frame_left_two,text="View Metadata",height = 3, width =20,command=lambda:get_metadata())
        #componants grid for frame 1
        self.frame_left_btn12.grid(row=0,column=0,sticky="nsew")
        self.frame_left_btn22.grid(row=1,column=0,sticky="nsew")



        #frame for componants for fisrt labeled frame  row configure  2
        self.frame_right.grid_rowconfigure(0, weight=1)   
        self.frame_right.columnconfigure(0, weight=1)
        #componants for frame 2
        self.scroll_text = ScrolledText(self.frame_right,bg="white",fg="green")
        #componants grid for frame 2
        self.scroll_text.grid(row=0,column=0,sticky="nsew")
                      
        self.scroll_text.insert(tk.INSERT,"-------------LOGS----------\nStarting...\n")

        

        def select_location_txt():
            self.location_file = select_file()
            self.scroll_text.insert(tk.INSERT,"Selected location file : " + self.location_file)
            self.scroll_text.insert(tk.INSERT,"\n")

        def choose_img_directory():
            directory = askdirectory()
            if not directory:
                self.scroll_text.insert(tk.INSERT,"Selected directory not valid : "  + directory)
                self.scroll_text.insert(tk.INSERT,"\n")
            else:
                self.image_dir = directory
                self.scroll_text.insert(tk.INSERT,"Selected Images  directory : "  + self.image_dir)
                self.scroll_text.insert(tk.INSERT,"\n")
    
        def choose_dest_directory():
            directory = askdirectory()
            if not directory:
                self.scroll_text.insert(tk.INSERT,"Selected destination not valid : "  + directory)
                self.scroll_text.insert(tk.INSERT,"\n")
            else:
                self.destination_dir = directory
                self.scroll_text.insert(tk.INSERT,"Selected destination  directory : "  + self.destination_dir)
                self.scroll_text.insert(tk.INSERT,"\n")

        def add_geotag():
            self.scroll_text.insert(tk.INSERT,"Geo tag adding.............. : \n")
            add_geo_tag(self.destination_dir,self.location_file,self.image_dir)
            self.scroll_text.insert(tk.INSERT,"\n")
            self.scroll_text.insert(tk.INSERT,"Geo tag adding completed! \n")

        def get_metadata():
            self.scroll_text.insert(tk.INSERT,"--------------Metadata--------------- : ")
            self.scroll_text.insert(tk.INSERT,"\n")
            

        def select_file():
            filetypes = (
                ('All files', '*.*'),
                ('text files', '*.txt')
            )

            filename = askopenfilename(
                title='Open a file',
                initialdir='/',
                filetypes=filetypes)

            showinfo(
                title='Selected File',
                message=filename
            )

            return filename


        def add_geo_tag(output_img_folder,location_file,img_location):
            # check the destination folder for the newly created images already exist or not.
            # if not exist create it.
            if not os.path.exists(output_img_folder):
                os.mkdir(output_img_folder)
                self.scroll_text.insert(tk.INSERT,"Creating Folder: "+output_img_folder+", your geotagged images will be saved there")
                self.scroll_text.insert(tk.INSERT,"\n")


            # read data from location.txt file
            file = open(location_file).readlines()

            for lines in file:
                if lines.startswith("#"):
                    continue
                else:
                    lines = lines.split(" ")

                # add geo tags to image (Longitude,Latitude,Altitude)
                geo_tag_adder(output_img_folder,img_location,str(lines[0]), float(lines[1]), float(lines[2]), float(lines[3]))
                # print metadata of newly created images
                #print_metadata(output_img_folder+str(lines[0]))

        # add geo tag to image
        def geo_tag_adder(output_img_folder,img_path,image_name, lat, lon,alt):
            # output image filename with relative path
            destination = output_img_folder+dir_seperator+image_name

            # check the image given found or not 
            if not os.path.exists(img_path+dir_seperator+image_name):
                self.scroll_text.insert(tk.INSERT,"Image not Found! Image Name:" + image_name)
                self.scroll_text.insert(tk.INSERT,"\n")
                return 0
            
            # write geo tags
            with open(img_path+dir_seperator+image_name, 'rb') as img_file:
                img = Image(img_file)
                img.gps_longitude = lon
                img.gps_latitude = lat
                img.gps_altitude = alt
                # save copy of image in destination folder
                with open(destination, 'wb') as new_image_file:
                    new_image_file.write(img.get_file())
                    self.scroll_text.insert(tk.INSERT,"Image generated :" + destination)
                    self.scroll_text.insert(tk.INSERT,"\n")



hack_gui = Convert(root)
root.mainloop()
